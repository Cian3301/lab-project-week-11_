<%-- 
    Document   : hello
    Created on : 1 Dec 2020, 22:14:51
    Author     : SWoodworth
--%>

<%@page import="com.mycompany.javamavenwebapp.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello from the JSP Page</h1>
        <p><%= new java.util.Date() %></p>
        <p><%= new Calculator().add(3, 4) %>
        <p><%= new Calculator().subtract(3, 4) %>
        <p><%= new Calculator().multiply(3, 4) %>
        <p><%= new Calculator().divide(3, 4) %>
    </body>
</html>
